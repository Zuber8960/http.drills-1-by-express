const express = require('express');
const fs = require("fs");
const requestId = require("express-request-id");


const app = express();
const router = require("./router/index.cjs");

const { PORT } = require("./config");
app.use(requestId());


app.use((req, res, next) => {
    const date = `${new Date().getDate()}/${new Date().getMonth()+1}/${new Date().getFullYear()}`;
    const options = {
        timeZone: 'Asia/Kolkata',
        hour12: true,
        hour: 'numeric',
        minute: 'numeric',
        second: 'numeric'
      };
    const time = (new Date()).toLocaleString('en-US', options);

    // const time = `${new Date().getHours()}/${new Date().getMinutes()+1}/${new Date().getSeconds()}`;

    const content = JSON.stringify({
        id : req.id,
        url : req.url,
        date : date,
        time : time
    });
    fs.appendFile("log.log", content+"\n", (err, data) => {
        if(err){
            console.error(err);
        }
    })
    next();
});

app.use(router);


app.listen(PORT, () => {
    console.log("Server is live !");
});