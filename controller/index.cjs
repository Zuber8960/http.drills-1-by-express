const uuid = require("uuid");
const http = require("http");
const path = require("path");
const fs = require("fs");


const logPath = path.join(__dirname, "../", "log.log");

exports.htmlController = (req, res, next) => {
    res.status(200).send(`
    <!DOCTYPE html>
    <html>

    <head>
    </head>

    <body>
        <h1>Any fool can write code that a computer can understand. Good programmers write code that humans can understand.
        </h1>
        <p> - Martin Fowler</p>
    </body>

    </html>
    `);
};


exports.jsonController = (req, res, next) => {
    res.status(200).json(
        {
            "slideshow": {
                "author": "Yours Truly",
                "date": "date of publication",
                "slides": [
                    {
                        "title": "Wake up to WonderWidgets!",
                        "type": "all"
                    },
                    {
                        "items": [
                            "Why <em>WonderWidgets</em> are great",
                            "Who <em>buys</em> WonderWidgets"
                        ],
                        "title": "Overview",
                        "type": "all"
                    }
                ],
                "title": "Sample Slide Show"
            }
        }
    );
};

exports.uuidController = (req, res, next) => {
    res.status(200).json({
        uuid: uuid.v4()
    });
}

exports.statusController = (req, res, next) => {
    const { status_code } = req.params;
    if (http.STATUS_CODES[status_code] === undefined || status_code == 100) {

        next(
            {
                status: 400,
                message: "Please pass some valid status code"
            }
        );
    } else {
        res.status(status_code).json(
            {
                status: status_code,
                message: http.STATUS_CODES[status_code]
            }
        );
    }

};


exports.delayController = (req, res, next) => {
    const { time } = req.params;


    if (time < 0 || isNaN(time) === true) {
        next(
            {
                status: 400,
                message: `Invalid Endpoint ! Please enter some valid endpoint after delay/ `
            }
        )
    } else {
        setTimeout(() => {
            res.status(200).send(`
            <!DOCTYPE html>
            <html>
                <body style="background-color: lightcoral;">
                    <h1> Sorry for delaying ${time} seconds. </h1>
                </body>
            </html>
            `);
        }, time * 1000);
    }

};


exports.homeController = (req, res, next) => {
    res.status(200).send(`
    <!DOCTYPE html>
    <html>

    <body style="background-color: lightcoral; color: #fff">
        <h1> Please click or enter any given endpoint. </h1>
        <hr>
        <ol>
            <li><a href="/html"> /html </a></li>
            <li><a href="/json"> /json </a></li>
            <li><a href="/uuid"> /uuid </a></li>
            <li><a href="/status/200"> /status/(statusCode) </a></li>
            <li><a href="/delay/1"> /delay/(delayInSeconds) </a></li>
        </ol>
    </body>

    </html>
    `);
}


exports.logController = (req, res, next) => {
    res.status(200).sendFile(logPath);
};

exports.errorHandler = (err, req, res, next) => {
    res.status(err.status).json({
        error: err.message
    });
};

exports.errorController = (req, res, next) => {
    res.status(404).send(`
    <!DOCTYPE html>
    <html>

    <body style="background-color: lightcoral; color: red">
        <h1> Error 404 : Page Not Found </h1>
    </body>

    </html>
    `);
}