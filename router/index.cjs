const express = require("express");

const router = express.Router();
const controller = require("../controller/index.cjs");

router.get("/html", controller.htmlController);
router.get("/json", controller.jsonController);
router.get("/uuid", controller.uuidController);
router.get("/status/:status_code", controller.statusController);
router.get("/delay/:time", controller.delayController);
router.get("/", controller.homeController);
router.get("/log", controller.logController);
router.use(controller.errorHandler);
router.use(controller.errorController);


module.exports = router;